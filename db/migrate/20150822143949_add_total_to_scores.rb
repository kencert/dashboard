class AddTotalToScores < ActiveRecord::Migration
  def change
    add_column :scores, :total, :float
  end
end
