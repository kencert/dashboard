class Signature < ActiveRecord::Base
  belongs_to :farmer
  belongs_to :form

  mount_uploader :signature, SignatureUploader
  validates_presence_of :signature
end
