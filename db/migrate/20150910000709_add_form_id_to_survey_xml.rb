class AddFormIdToSurveyXml < ActiveRecord::Migration
  def change
    add_reference :survey_xmls, :form, index: true, foreign_key: true
  end
end
