class CreateSubmissions < ActiveRecord::Migration
  def change
    create_table :submissions do |t|
      t.references :form, index: true, foreign_key: true
      t.string :file
      t.string :device_id

      t.timestamps null: false
    end
  end
end
