json.array!(@questions) do |question|
  json.extract! question, :id, :question, :question_name
  json.url question_url(question, format: :json)
end
