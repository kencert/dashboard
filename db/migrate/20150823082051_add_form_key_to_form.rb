class AddFormKeyToForm < ActiveRecord::Migration
  def change
    add_column :forms, :odk_key, :string, unique: true
  end
end
