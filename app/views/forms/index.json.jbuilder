json.array!(@forms) do |form|
  json.extract! form, :id, :farmer_id, :form_name, :date
  json.url form_url(form, format: :json)
end
