class Score < ActiveRecord::Base
  belongs_to :form
  belongs_to :farmer

  validates_presence_of :total, :maximum_total

  PASSED = "passed"
  FAILED = "failed"

  def self.locate_score(farmer_id, form_id)
    Score.where("farmer_id = :farmer_id and form_id = :form_id",  {farmer_id: farmer_id, form_id: form_id})
  end

  def state
    self.total/self.maximum_total * 100  < 75 ? FAILED : PASSED
  end
end
