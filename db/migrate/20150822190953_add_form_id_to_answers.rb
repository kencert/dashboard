class AddFormIdToAnswers < ActiveRecord::Migration
  def change
    add_column :answers, :form_id, :integer
    add_index :answers, :form_id
  end
end
