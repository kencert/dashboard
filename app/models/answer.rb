class Answer < ActiveRecord::Base
  belongs_to :question
  belongs_to :farmer
  belongs_to :form
end
