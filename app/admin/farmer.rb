ActiveAdmin.register Farmer do
	menu priority: 3, label: proc{ "Farmers" }
  
  preserve_default_filters!
  remove_filter :assignments, :scores, :answers, :updated_at, :signature, :survey_file, :signatures
  
	controller do 
		def permitted_params
			params.permit(farmer: [:name, :membership_number, :telephone])
		end
	end

	show do 
		attributes_table_for resource do 
      row :id
      row :name
      row :membership_number
      row :telephone
      row :created_at
      row :updated_at
      row("Participated Surveys") {|resource| resource.forms.count}
    end

		panel "Participated Surveys" do 
			farmer_id = resource.id

			table_for resource.forms do
        column("Name") {|form| link_to(form.name, admin_form_path(form))}
        column("Answers") { |form| link_to("answers", farmer_form_question_answers_admin_form_path(form, farmer_id: farmer_id))}
        column("Score") do |form|
          score = Score.where(
              "farmer_id = :farmer_id and form_id = :form_id", 
              {farmer_id: farmer_id, form_id: form.id}
              ).first
          if !score.blank? 
            status_tag(score.state)
          else
          	"Scores Not Found"
          end
        end
        column :created_at
			end
		end
	end

	index do 
		column("MemberShip Number") {|farmer| link_to(farmer.membership_number, admin_farmer_path(farmer)) }
		column :name
		column "Latest Survey Participated" do |farmer|
			farmer.forms.first ? farmer.forms.first.name : "NaN"
		end
	end

end
