class AddMaximumTotalToScore < ActiveRecord::Migration
  def change
    add_column :scores, :maximum_total, :float
    add_index :scores, [:maximum_total, :total]
  end
end
