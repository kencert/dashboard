class FarmersController < InheritedResources::Base
  skip_before_filter :verify_authenticity_token, only: [:receive_answers]

  def receive_answers
    input = params.keys.map(&:to_sym)

    submitted_file = params[input.first]
    submitted_signature = params[input[1]]
    device_id = params[input[2]]

    status = Farmer.parse_content(submitted_file, submitted_signature, device_id)
    if status == "success"
      head :created
    elsif status == "form parsing error"
      head :unprocessable_entity
    end
  end

  private

    def farmer_params
      params.require(:farmer).permit(:name, :membership_number, :telephone, :xml_submission_file, :deviceID)
    end
end

