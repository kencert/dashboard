class PagesController < ApplicationController
  def error_404
    #render status: :not_found
    render_404
  end

private

  def render_404
    respond_to do |format|
      format.html { render file: "#{Rails.root}/public/404", layout: false, status: :not_found}
      format.any {head :not_found}
    end
  end
end
