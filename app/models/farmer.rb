class Farmer < ActiveRecord::Base
	has_many :assignments, dependent: :destroy
	has_many :forms, through: :assignments
  has_many :scores, dependent: :destroy
  has_many :answers, dependent: :destroy
  has_many :signatures, dependent: :destroy

  validates_presence_of :membership_number, :name, :telephone
  validates_uniqueness_of :membership_number, :telephone

  class << self 
    def parse_content(answers_file, signature_file, device_id)
      if get_answers(answers_file, signature_file, device_id)
        "success"
      else
        "form parsing error"
      end
    end

    def get_answers(answers_file, signature_file, device_id)
      file_contents = File.open(answers_file.tempfile).read
      answers_hash = Hash.from_xml(file_contents)
      answers = parse_answers(answers_hash)
      dirty_answers_hash = answers.first
      unsorted_answers = answers[1]
      observations = answers.last
      assign_answers(unsorted_answers, observations, dirty_answers_hash, signature_file, answers_file, device_id)
    end

    def parse_answers(answers_hash)
      observations = {}
      unsorted_answers = {}
      main_key = answers_hash.keys.first
      dirty_answers_hash = answers_hash[main_key].except("id")
      dirty_answers_hash.each do |k, v|
        if v.class == Hash 
          v_keys = v.keys
          v.each do |key, value|
            common_key = "o"
            question_key = "note"
            if key.include? common_key
              if key.first == common_key
                observations[key] = value
              elsif key.include? question_key
                unsorted_answers[key] = value 
              end
            end
          end
        end
      end
      answers = [dirty_answers_hash, unsorted_answers, observations]
    end

    def assign_answers(unsorted_answers, observations, dirty_answers_hash, signature_file, submission_file, device_id)
      farmer = {}
      farmer[:membership_number] = unsorted_answers["notea2"]
      unsorted_answers.each do |unsorted_answer|
        unsorted_answer = Hash[unsorted_answer.first, unsorted_answer.last]
        if unsorted_answer["notea1"] != nil
          farmer[:name] = unsorted_answer["notea1"]
        elsif unsorted_answer["notea3"] != nil 
          farmer[:telephone] = unsorted_answer["notea3"]
        end
      end
      
      form_odk_key = dirty_answers_hash.keys[1]   
      form = Form.find_by( odk_key: form_odk_key )


      if form 
        farmer = form.farmers.create_with(
            name: farmer[:name],
            telephone: farmer[:telephone]
          ).find_or_create_by!(membership_number: farmer[:membership_number])
        select_questions(farmer, form, unsorted_answers, observations, signature_file, submission_file, device_id) if form
      else
        raise "Form does not exist"
      end    
    end

    def select_questions(farmer, form, unsorted_answers, observations, signature_file, submission_file, device_id)
      unsorted_answers.each do |key, value|
        question = form.questions.find_by(name: key)
        full_description = ""
        if question 
          observation_key = key[4..-1]
          observation_key.insert(0,"o")
          description = observations.keys.select{ |key| key == observation_key }
          
          unless description.empty?
            observation_index = description.first
            full_description = observations[observation_index]
          end
        
          answer = form.questions.find(question.id).answers.create(
              answer: unsorted_answers[key],
              farmer_id: farmer.id,
              description: full_description,
              form_id: form.id
            )
        end
      end
      farmer.signatures.create(form_id: form.id, signature: signature_file )
      form.submissions.create(device_id: device_id, file: submission_file )
      assign_answers(farmer, form, unsorted_answers)
    end

    def assign_scores(farmer, form, unsorted_answers)
      unwanted_keys = %w( a m )
      unsorted_answers.each do |key, value|
        if unwanted_keys.any? { |unwanted_key| key.include? unwanted_key }
          unsorted_answers.except!(key)
        end
      end

      maximum_total = unsorted_answers.count
      total = unsorted_answers.values.count{ |unsorted_answer| unsorted_answer == "yes" }

      form.scores.create_with(
          farmer_id: farmer.id,
          total: total,
          maximum_total: maximum_total
        ).find_or_create_by(farmer_id: farmer_id, form_id: form.id )
    end
  end

end