class CreateSurveyXmls < ActiveRecord::Migration
  def change
    create_table :survey_xmls do |t|
      t.string :survey_xml

      t.timestamps null: false
    end
  end
end
