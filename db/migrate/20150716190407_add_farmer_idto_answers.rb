class AddFarmerIdtoAnswers < ActiveRecord::Migration
  def change
    add_column :answers, :farmer_id, :integer, after: :question_id
  end
end
