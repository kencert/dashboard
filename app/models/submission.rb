class Submission < ActiveRecord::Base
  belongs_to :form
  mount_uploader :file, SubmissionUploader

  validates_presence_of :device_id, :file
end
