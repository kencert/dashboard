class CreateQuestions < ActiveRecord::Migration
  def change
    create_table :questions do |t|
      t.references :form, index: true, foreign_key: true
      t.string :name
      t.string :content

      t.timestamps null: false
    end
    add_index :questions, :name
    add_index :questions, :content
  end
end
