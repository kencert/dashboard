class Form < ActiveRecord::Base
	has_many :assignments
	has_many :farmers, through: :assignments
  has_many :scores, dependent: :destroy
  has_many :questions, dependent: :destroy
  has_many :answers, dependent: :destroy
  has_many :signatures, dependent: :destroy
  has_one :survey_xml, dependent: :destroy

  validates_presence_of :name
  validates_uniqueness_of :name

  def self.import(file)
    spreadsheet = open_spreadsheet(file)
    questions_sheet = spreadsheet.sheet(0)
    unfiltered_questions = questions_sheet.parse(name: 'name', content: 'label')
    unfiltered_questions.reject!(&:blank?)
    clean_questions = []
    form_name_hash = unfiltered_questions.find do |h|
      h[:name] == "ccsic"
    end
    form_name = form_name_hash[:content]
    odk_key = form_name_hash[:name]
    if form_name
      unfiltered_questions.each do |question|
        clean_questions.push sanitizer(question)
      end
    else
      raise FormNotSet
    end
    clean_questions.compact!
    form_attributes = { form_name: form_name, odk_key: odk_key }
    save_form(form_attributes, clean_questions)
  end

  def self.save_form(form_attributes, clean_questions)
    form = Form.where(odk_key: form_attributes[:odk_key])
    if !form.blank?
      puts "Form already exists"
    else
      form = Form.create(name: form_attributes[:form_name], odk_key: form_attributes[:odk_key])
      clean_questions.each do |question|
        form.questions.create!(question)
      end
    end
  end
  
  def self.sanitizer(question)
    if question[:name].to_s.blank? == false && question[:name].to_s.include?('note')
      question
    end
  end

  def self.open_spreadsheet(file)
    case File.extname(file.original_filename)
    when ".csv" then Roo::Csv.new(file.path)
    when ".xls" then Roo::Excel.new(file.path)
    when ".xlsx" then Roo::Excelx.new(file.path)
    when ".ods" then Roo::OpenOffice.new(file.path)
    else raise "Unknown file type: #{file.original_filename}"
    end
  end
end
