ActiveAdmin.register Form do
  menu priority: 2, label: proc{ "Survey" }

  show title: :name do 
    form.name
  end

  member_action :farmer_form_question_answers do 
    questions = resource[:form]
    farmer = resource[:farmer]
  end

  member_action :form_passed do 
    form = resource
  end

  member_action :form_failed do 
    form = resource
  end

  collection_action :passed do 
    @forms = Form.all
  end

  filter :id
  filter :name
  filter :created_at

  permit_params :name, :date

  show do 
    
    panel "Farmers" do 
      form_id = resource.id

      table_for resource.farmers do
        column("MemberShip Number", sortable: :id) {|farmer| link_to(farmer.membership_number, admin_farmer_path(farmer)) }
        column :name
        column("Score") do |farmer|
          score = Score.where(
              "farmer_id = :farmer_id and form_id = :form_id", 
              {farmer_id: farmer.id, form_id: form_id}
              ).first
          if !score.blank? 
            status_tag(score.state)
          else
            "Scores Not Found"
          end
        end 
      end 
    end

    panel "Questions" do 
      render partial: "form_questions"
    end
  end

  sidebar "Details", only: :show do 
    attributes_table_for resource do 
      row :id
      row :name
      row :created_at
      row("Farmer No.") {|form| form.farmers.all.count }
      row("Questions:") {|form| form.questions.all.count }
    end
  end

  sidebar "Filters", only: :show do 
    strong { link_to "Passed Farmers", form_passed_admin_form_path(resource) }
    strong { link_to "Failed Farmers", form_failed_admin_form_path(resource) }
  end
  
end
