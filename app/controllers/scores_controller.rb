class ScoresController < InheritedResources::Base

  private

    def score_params
      params.require(:score).permit(:form_id, :major_must, :minor_must, :total)
    end
end

