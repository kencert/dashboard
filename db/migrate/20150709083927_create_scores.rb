class CreateScores < ActiveRecord::Migration
  def change
    create_table :scores do |t|
      t.references :form, index: true, foreign_key: true
      t.float :major_must
      t.float :minor_must
      t.float :total

      t.timestamps null: false
    end
  end
end
