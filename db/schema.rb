# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150910000709) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "active_admin_comments", force: :cascade do |t|
    t.string   "namespace"
    t.text     "body"
    t.string   "resource_id",   null: false
    t.string   "resource_type", null: false
    t.integer  "author_id"
    t.string   "author_type"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "active_admin_comments", ["author_type", "author_id"], name: "index_active_admin_comments_on_author_type_and_author_id", using: :btree
  add_index "active_admin_comments", ["namespace"], name: "index_active_admin_comments_on_namespace", using: :btree
  add_index "active_admin_comments", ["resource_type", "resource_id"], name: "index_active_admin_comments_on_resource_type_and_resource_id", using: :btree

  create_table "admin_users", force: :cascade do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
  end

  add_index "admin_users", ["email"], name: "index_admin_users_on_email", unique: true, using: :btree
  add_index "admin_users", ["reset_password_token"], name: "index_admin_users_on_reset_password_token", unique: true, using: :btree

  create_table "answers", force: :cascade do |t|
    t.integer  "question_id"
    t.string   "answer"
    t.string   "description"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.integer  "farmer_id"
    t.integer  "form_id"
  end

  add_index "answers", ["form_id"], name: "index_answers_on_form_id", using: :btree
  add_index "answers", ["question_id"], name: "index_answers_on_question_id", using: :btree

  create_table "assignments", force: :cascade do |t|
    t.integer  "form_id"
    t.integer  "farmer_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "assignments", ["farmer_id"], name: "index_assignments_on_farmer_id", using: :btree
  add_index "assignments", ["form_id"], name: "index_assignments_on_form_id", using: :btree

  create_table "farmers", force: :cascade do |t|
    t.string   "name"
    t.string   "membership_number"
    t.string   "telephone"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
    t.string   "survey_file"
  end

  add_index "farmers", ["membership_number", "name"], name: "index_farmers_on_membership_number_and_name", using: :btree

  create_table "forms", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string   "odk_key"
  end

  create_table "questions", force: :cascade do |t|
    t.integer  "form_id"
    t.string   "name"
    t.string   "content"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "questions", ["content"], name: "index_questions_on_content", using: :btree
  add_index "questions", ["form_id"], name: "index_questions_on_form_id", using: :btree
  add_index "questions", ["name"], name: "index_questions_on_name", using: :btree

  create_table "scores", force: :cascade do |t|
    t.integer  "form_id"
    t.float    "major_must"
    t.float    "minor_must"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
    t.integer  "farmer_id"
    t.float    "total"
    t.float    "maximum_total"
  end

  add_index "scores", ["form_id"], name: "index_scores_on_form_id", using: :btree
  add_index "scores", ["maximum_total", "total"], name: "index_scores_on_maximum_total_and_total", using: :btree

  create_table "signatures", force: :cascade do |t|
    t.integer  "farmer_id"
    t.integer  "form_id"
    t.string   "signature"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "signatures", ["farmer_id"], name: "index_signatures_on_farmer_id", using: :btree
  add_index "signatures", ["form_id"], name: "index_signatures_on_form_id", using: :btree

  create_table "submissions", force: :cascade do |t|
    t.integer  "form_id"
    t.string   "file"
    t.string   "device_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "submissions", ["form_id"], name: "index_submissions_on_form_id", using: :btree

  create_table "survey_xmls", force: :cascade do |t|
    t.string   "survey_xml"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer  "form_id"
  end

  add_index "survey_xmls", ["form_id"], name: "index_survey_xmls_on_form_id", using: :btree

  create_table "surveys", force: :cascade do |t|
    t.integer  "form_id"
    t.string   "survey_xml"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "surveys", ["form_id"], name: "index_surveys_on_form_id", using: :btree

  create_table "users", force: :cascade do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

  create_table "views", force: :cascade do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
  end

  add_index "views", ["email"], name: "index_views_on_email", unique: true, using: :btree
  add_index "views", ["reset_password_token"], name: "index_views_on_reset_password_token", unique: true, using: :btree

  add_foreign_key "answers", "questions"
  add_foreign_key "assignments", "farmers"
  add_foreign_key "assignments", "forms"
  add_foreign_key "questions", "forms"
  add_foreign_key "scores", "forms"
  add_foreign_key "signatures", "farmers"
  add_foreign_key "signatures", "forms"
  add_foreign_key "submissions", "forms"
  add_foreign_key "survey_xmls", "forms"
  add_foreign_key "surveys", "forms"
end
