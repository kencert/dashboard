class RenameformNameinforms < ActiveRecord::Migration
  def change
     rename_column :forms, :form_name, :name
  end
end
