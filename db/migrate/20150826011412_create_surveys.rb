class CreateSurveys < ActiveRecord::Migration
  def change
    create_table :surveys do |t|
      t.references :form, index: true, foreign_key: true
      t.string :survey_xml

      t.timestamps null: false
    end
  end
end
