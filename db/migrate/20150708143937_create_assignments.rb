class CreateAssignments < ActiveRecord::Migration
  def change
    create_table :assignments do |t|
      t.references :form, index: true, foreign_key: true
      t.references :farmer, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
