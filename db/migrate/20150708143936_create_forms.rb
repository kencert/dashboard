class CreateForms < ActiveRecord::Migration
  def change
    create_table :forms do |t|
      # t.references :farmer, index: true, foreign_key: true
      t.string :form_name
      t.datetime :date

      t.timestamps null: false
    end
  end
end
