class CreateSignatures < ActiveRecord::Migration
  def change
    create_table :signatures do |t|
      t.references :farmer, index: true, foreign_key: true
      t.references :form, index: true, foreign_key: true
      t.string :signature

      t.timestamps null: false
    end
  end
end
