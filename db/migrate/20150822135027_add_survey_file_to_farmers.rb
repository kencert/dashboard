class AddSurveyFileToFarmers < ActiveRecord::Migration
  def change
    add_column :farmers, :survey_file, :string
  end
end
