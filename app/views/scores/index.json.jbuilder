json.array!(@scores) do |score|
  json.extract! score, :id, :form_id, :major_must, :minor_must, :total
  json.url score_url(score, format: :json)
end
