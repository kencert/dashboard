class SurveyXmlsController < ApplicationController
  def getforms
    @forms = SurveyXml.all 
    respond_to do |format|
      format.all { head :xml => @forms, content_type: "text/xml", :status => 200}
    end
  end
end
