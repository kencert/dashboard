json.array!(@farmers) do |farmer|
  json.extract! farmer, :id, :farmer_name, :membership_number, :telephone
  json.url farmer_url(farmer, format: :json)
end
