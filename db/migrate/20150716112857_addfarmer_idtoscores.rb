class AddfarmerIdtoscores < ActiveRecord::Migration
  def change
    add_column :scores, :farmer_id, :integer, after: :form_id
  end
end
