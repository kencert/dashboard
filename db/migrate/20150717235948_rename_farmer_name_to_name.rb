class RenameFarmerNameToName < ActiveRecord::Migration
  def change
    rename_column :farmers, :farmer_name, :name
  end
end
