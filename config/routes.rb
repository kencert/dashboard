Rails.application.routes.draw do
    
  resources :forms do 
    collection do 
      post :import 
    end
  end

  # resources :survey do 
  #   collection { post :import_xml }
  # end

  resources :farmers do 
    resources :forms do 
      resources :questions do 
        resources :answers
      end
      resources :scores
    end
  end

  root to: "admin/dashboard#index"

  match '/submission' => 'farmers#receive_answers', as: 'submission', via: :post
  match '/formList' => 'survey_xmls#getforms', as: 'formList', via: :get
  

  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)
  devise_for :users
  devise_for :views

  match '*path', to: 'pages#error_404', via: :all
end
