class FormsController < InheritedResources::Base

  def getforms
    @forms = Form.all
    render xml: @forms, content_type: "text/xml"
  end

  def import
    Form.import(params[:file])
    redirect_to root_url, notice: "Survey Questions Imported"
  end

  private

    def form_params
      params.require(:form).permit(:farmer_id, :name, :odk_key)
    end
end

