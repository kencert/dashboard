class CreateFarmers < ActiveRecord::Migration
  def change
    create_table :farmers do |t|
      t.string :farmer_name
      t.string :membership_number, unique: true
      t.string :telephone

      t.timestamps null: false
    end
    add_index :farmers, [:membership_number, :farmer_name]
  end
end
