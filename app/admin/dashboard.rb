ActiveAdmin.register_page "Dashboard" do

  menu priority: 1, label: proc{ I18n.t("active_admin.dashboard") }

  content title: proc{ I18n.t("active_admin.dashboard") } do

    columns do 
      column do 
        panel "Recent" do 
          table_for Farmer.order("created_at desc").limit(10) do 
            column("Farmer Name") { |farmer| farmer.name }
            column("MemberShip Number") {|farmer| link_to(farmer.membership_number, admin_farmer_path(farmer)) }
            column("Survey Name") { |farmer| farmer.forms.last ? farmer.forms.last.name : "NaN" }
          end
          strong { link_to "View All Farmers", admin_farmers_path }
        end
      end
    end

  end 
end
